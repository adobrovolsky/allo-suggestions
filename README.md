### Start script
```
python ./allo-suggestion.py --verbose --threads 20
```


### Check result
```
sqlite3 ./suggestion.db

Все рекомендации сохраняются в таблицу suggestions.
select * from suggestions;

Все возможные комбинации букв сохраняются в таблицу search_terms при первом запуске скрипта.
select * from search_terms;

После сохранения всех рекомендаций для определенной комбинации, ее статус меняется с 0 на 1.
Посмотреть все комбинации, для которых получены рекомендации можно так:
select * from search_terms where processed = 1;
select count(id) from search_terms where processed = 1;
```