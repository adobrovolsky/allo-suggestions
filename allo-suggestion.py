import string
import requests
import sqlite3
import pickle
import argparse
from time import time
from os.path import isfile
from math import pow
from threading import Lock
from concurrent.futures import ThreadPoolExecutor, wait

URL = "https://allo.ua/ua/catalogsearch/ajax/suggest/?currentTheme=main&currentLocale=uk_UA"
verbose = False


class SuggestionDao:
    __suggestion_lock = Lock()
    __term_lock = Lock()
    db_init = False
    suggestion_count = 0
    term_count = 0

    def __init__(self, db_name="suggestion.db"):
        self.__db_name = db_name
        self.db_init = not isfile(self.__db_name)
        with sqlite3.connect(self.__db_name) as conn:
            conn.executescript(
                """CREATE TABLE IF NOT EXISTS suggestions      
                (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    name TEXT NOT NULL,
                    term_id INTEGER NOT NULL,
                    FOREIGN KEY(term_id) REFERENCES search_terms(id)
                );
                CREATE TABLE IF NOT EXISTS search_terms
                (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    name TEXT NOT NULL,
                    processed INTEGER NOT NULL
                );"""
            )

    def save_suggestions(self, suggestions, term_id):
        with sqlite3.connect(self.__db_name) as conn:
            for item in suggestions:
                conn.execute("INSERT INTO suggestions(name, term_id) VALUES(?, ?)", (item, term_id))
                with self.__suggestion_lock:
                    self.suggestion_count += 1
            conn.execute("UPDATE search_terms SET processed = ? WHERE id = ?", (1, term_id))
            with self.__term_lock:
                self.term_count += 1

    def save_searchterms(self, terms):
        with sqlite3.connect(self.__db_name) as conn:
            for item in terms:
                conn.execute("INSERT INTO search_terms(name, processed) VALUES(?, ?)", (item, 0))

    def get_searchterms(self):
        result = []
        with sqlite3.connect(self.__db_name) as conn:
            cursor = conn.execute("SELECT * FROM search_terms WHERE processed == 0")
            for row in cursor:
                result.append({"id": row[0], "name": row[1], "processed": row[2]})

        return result


class TermGenerator:
    __chars = string.ascii_lowercase
    __dump_file = ".generator.dump"

    def __init__(self):
        self.__indexes = self.load()

    def get2(self):
        return ["ha", "hu", "sas"]

    def get(self):
        alphabet = self.__chars
        size = len(alphabet)
        start = max(self.__indexes[3], 0)
        end = int(pow(size, 3))
        a, b, c = self.__indexes[0:3]

        for i in range(start, end):
            self.__indexes[3] = i
            a = (a + 1) % size
            self.__indexes[0] = a

            if i % size == 0:
                b = (b + 1) % size
                self.__indexes[1] = b

            if i % pow(size, 2) == 0:
                c = (c + 1) % size
                self.__indexes[2] = c

            yield alphabet[c] + alphabet[b] + alphabet[a]

    def dump(self):
        with open(self.__dump_file, "wb") as stream:
            stream.write(pickle.dumps(self.__indexes))

    def load(self):
        stream = None
        try:
            stream = open(self.__dump_file, "rb")
            res = stream.read(88)
            return pickle.loads(res)
        except FileNotFoundError as e:
            return [-1] * 6
        finally:
            if stream is not None:
                stream.close()


class SuggestionService:

    def __init__(self, url, suggestion_dao: SuggestionDao, term_generator: TermGenerator):
        self.__endpoint = url
        self.__suggestion_dao = suggestion_dao
        self.__term_generator = term_generator
        if self.__suggestion_dao.db_init:
            self.__suggestion_dao.save_searchterms(self.__term_generator.get())

    def save_suggestions(self, search_term):
        try:
            result = self.get_suggestions(search_term)
            self.__suggestion_dao.save_suggestions(result, search_term["id"])
        except requests.HTTPError as e:
            print("Error getting suggestions for {}".format(search_term["name"]) + str(e))
        else:
            if verbose:
                print("Suggestions for {}: {}".format(search_term["name"], result))

    def get_suggestions(self, search_term):
        data = {'q': search_term["name"]}
        response = requests.post(self.__endpoint, data)
        if response.status_code != requests.codes.ok:
            response.raise_for_status()
        content = response.json()

        if content and "query" in content:
            return content["query"]
        else:
            return []

    def get_suggestioncount(self):
        return self.__suggestion_dao.suggestion_count

    def get_termcount(self):
        return self.__suggestion_dao.term_count

    def get_searchterms(self):
        return self.__suggestion_dao.get_searchterms()


def read_args():
    parser = argparse.ArgumentParser(description=""""Get search suggestions from allo.ua""",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v", "--verbose", help="Enables detailed logging", action="store_true")
    parser.add_argument("-t", "--threads", default=20, type=int, help="Max number of threads")
    return parser.parse_args()


def main():
    cli_args = read_args()
    start_time = time()
    global verbose
    verbose = cli_args.verbose
    suggestion_service = SuggestionService(URL, SuggestionDao(), TermGenerator())
    executor = ThreadPoolExecutor(cli_args.threads)
    search_terms = suggestion_service.get_searchterms()

    try:
        if search_terms:
            print("It will be processed {} search terms".format(len(search_terms)))
            future_list = executor.map(suggestion_service.save_suggestions, search_terms)
            wait([future for future in future_list if future])

    except KeyboardInterrupt as e:
        print("Quitting...")
    finally:
        executor.shutdown(wait=True)

    end_time = time()

    if search_terms:
        print("-" * 64)
        print("Total search terms: " + str(suggestion_service.get_termcount()))
        print("Total suggestions: " + str(suggestion_service.get_suggestioncount()))
        print("Execution time: {:.2f}".format(end_time - start_time))
        print("-" * 64)
    else:
        print("All suggestions saved. See suggestion.db")


if __name__ == "__main__":
    main()
